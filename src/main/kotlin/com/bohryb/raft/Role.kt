package com.bohryb.raft

enum class Role {
    LEADER,
    CANDIDATE,
    FOLLOWER,
}