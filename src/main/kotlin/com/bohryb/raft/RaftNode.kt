package com.bohryb.raft

import com.bohryb.raft.commands.Command
import com.bohryb.raft.commands.Command.*
import com.bohryb.raft.commands.CommandQueue
import com.bohryb.raft.sminstructions.NoopSMInstruction
import java.util.concurrent.ThreadLocalRandom
import java.util.logging.Level
import java.util.logging.Logger

/**
 * Append Entity note.
 * Due to asynchronous nature of raft node communication appendEntity(Response) might be lost
 * or delivered out of order. If could affect correctness and performance. To keep algo correctness we
 * introduce the following properties.
 * 1) AppendEntity(Response) processing is idempotent.
 * 2) If request with sequence number n was processed no request with sequence number < n can be processed.
 * 3) If linearized AppendEntity(Response)s and r(k) and r(k+1) both appendEnity r(k) == r(k+1)
 * 4) Messages can be dropped
 */
class RaftNode(
    val inputCommands: CommandQueue<Command>,
    val outputCommands: CommandQueue<Command>,
    val config: Config
) {

    private var role: Role = Role.FOLLOWER
    private val log: Log = Log()
    private var term: Int = 0
    private var leaderAlias: String? = null
    private var heartbeat: Long = System.currentTimeMillis()


    @Volatile
    private var isStopped = false

    fun stop() {
        isStopped = true
    }

    fun start() {
        logger.info("Start RaftNode. ${config.instanceAlias}")
        while (!isStopped) {
            if (role === Role.FOLLOWER)
                doFollower(randomizeTimeout(config.heartbeatTimeoutMin, config.heartbeatTimeoutMax))
            if (role === Role.CANDIDATE)
                doCandidate(randomizeTimeout(config.electionTimeoutMin, config.electionTimeoutMax))
            if (role === Role.LEADER)
                doLeader()
        }
    }

    fun doFollower(timeout: Long) {
        logger.info("${config.instanceAlias} became follower. in term ${term}")

        heartbeat = System.currentTimeMillis();
        while (System.currentTimeMillis() - heartbeat < timeout && role == Role.FOLLOWER && !isStopped) {
            val command = inputCommands.get()
            if (command == null) continue

            when (command) {
                is RequestVote -> processRequestVote(command);
                is AppendEntity -> processAppendEntity(command)
            }
        }

        role = Role.CANDIDATE;
    }

    fun doCandidate(timeout: Long) {
        term++
        logger.info("${config.instanceAlias} became candidate. in term ${term}")
        heartbeat = System.currentTimeMillis();
        leaderAlias = null

        val votes: HashSet<String> = HashSet()
        votes.add(config.instanceAlias)

        broadcastRequestVotes()
        while (System.currentTimeMillis() - heartbeat < timeout && role == Role.CANDIDATE && !isStopped) {
            val command = inputCommands.get()
            if (command == null) continue

            when (command) {
                is RequestVote -> processRequestVote(command)
                is RequestVoteResponse -> processRequestVoteResponse(command, votes)
                is AppendEntity -> processAppendEntity(command)
            }
        }
    }

    fun doLeader() {
        logger.info("${config.instanceAlias} became leader. in term ${term}")
        val peersState: Map<String, Pair<Int, Int>> = HashMap();
        while (role == Role.LEADER && !isStopped) {
            broadcastAppendEntity(peersState)

            val command: Command = inputCommands.get();
            when (command) {
                is AppendEntity -> processAppendEntity(command)
            }
        }


    }

    fun processRequestVote(command: RequestVote) {
        if (shouldVoteForCandidate(command)) {
            heartbeat = System.currentTimeMillis();
            leaderAlias = command.sourceAlias;
            term = command.term;
            role = Role.FOLLOWER;
            sendRequestVoteResponse(command, true)
            logger.fine("${config.instanceAlias} voted for ${command.sourceAlias} in term ${term}")
            return
        }
        sendRequestVoteResponse(command, false)
    }

    fun processAppendEntity(command: AppendEntity) {
        logger.fine("${config.instanceAlias} got Append entity from ${command.sourceAlias}")
        if (isLeader(command)) {
            logger.fine("${config.instanceAlias} became follower of ${command.sourceAlias}")
            role = Role.FOLLOWER;
            term = command.leaderTerm;
            leaderAlias = command.sourceAlias;
            heartbeat = System.currentTimeMillis();
        }
    }

    fun processRequestVoteResponse(command: RequestVoteResponse, votes: HashSet<String>) {
        if (command.isVoted && command.term == term) {
            votes.add(command.sourceAlias)
            if (votes.size > config.peers.size / 2) {
                leaderAlias = config.instanceAlias
                role = Role.LEADER
            }
        }
        term = Math.max(term, command.term);
    }

    fun sendRequestVoteResponse(command: RequestVote, isVoted: Boolean) {
        outputCommands.put(
            RequestVoteResponse(
                command.sourceAlias,
                command.destAlias,
                term,
                isVoted
            )
        );
    }

    fun isLeader(command: AppendEntity): Boolean {
        return command.leaderTerm >= term;
    }

    fun shouldVoteForCandidate(command: RequestVote): Boolean {
        return command.term >= term && (command.lastLogTerm > log.lastTerm
                || (command.lastLogTerm == log.lastTerm && command.lastLogIndex >= log.lastIndex));
    }

    fun broadcastRequestVotes() {
        config.peers.keys.forEach {
            if (it != config.instanceAlias) {
                outputCommands.put(
                    RequestVote(
                        it,
                        config.instanceAlias,
                        term,
                        log.lastTerm,
                        log.lastIndex
                    )
                );
            }
        }
    }

    var lastAppendEntityTime: Long = System.currentTimeMillis();
    fun broadcastAppendEntity(peersState: Map<String, Pair<Int, Int>>) {
        if (System.currentTimeMillis() - lastAppendEntityTime > 30) {
            lastAppendEntityTime = System.currentTimeMillis()
            config.peers.keys.filter { !it.equals(config.instanceAlias) }.forEach {
                // heartbeat

                outputCommands.put(
                    AppendEntity(
                        it,
                        config.instanceAlias,
                        term,
                        NoopSMInstruction(),
                        log.getPenultimate().index,
                        log.getPenultimate().term
                    )
                )
            }
        }
    }

    private fun randomizeTimeout(min: Long, max: Long): Long {
        return ThreadLocalRandom.current().nextLong(min, max)
    }

    companion object RaftNode {
        val logger = run {
            val logger1 = Logger.getLogger(com.bohryb.raft.RaftNode.javaClass.simpleName)
            logger1.level = Level.INFO
            logger1
        }

    }
}



