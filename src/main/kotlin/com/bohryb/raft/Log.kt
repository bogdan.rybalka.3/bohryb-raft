package com.bohryb.raft

import com.bohryb.raft.exceptions.LogException
import com.bohryb.raft.sminstructions.NoopSMInstruction
import com.bohryb.raft.sminstructions.SMInstruction

class Log() {
    private val log: ArrayDeque<Entry> = ArrayDeque();
    var lastTerm: Int = 0
        private set
    var lastIndex: Int = 0
        get() = log.size - 1;
        private set

    init {
        log.addLast(Entry(NoopSMInstruction(), 0, 0))
        log.addLast(Entry(NoopSMInstruction(), 0, 0))
    }

    fun put(term: Int, instruction: SMInstruction): Unit {
        if (term < lastTerm)
            throw LogException("${term} is too small.");

        log.add(Entry(
            instruction,
            term,
            log.size
        ));
        lastTerm = term;
    }

    fun get(): Entry {
        return log.last();
    }

    fun get(index: Int): Entry {
        return log[index];
    }

    fun getPenultimate(): Entry {
        return log[log.size - 2];
    }

    fun remove() {
        if (log.size == 2)
            throw LogException("You can not remove first two instructions");

        log.removeLast();
    }

    fun removeUpTo(index: Int) {
        log.dropLastWhile { e -> e.index >= index }
    }

    fun isLastEntryMatch(term: Int, index: Int): Boolean {
        return get().index == index && term == get().term
    }

}

data class Entry(
    val instruction: SMInstruction,
    val term: Int,
    val index: Int
) {}
