package com.bohryb.raft.commands

import com.bohryb.raft.sminstructions.SMInstruction

interface Command {
    val destAlias: String
    val sourceAlias: String

    class AppendEntity(override val destAlias: String,
                       override val sourceAlias: String,
                       val leaderTerm: Int,
                       val instruction: SMInstruction,
                       val prevEntryIndex: Int,
                       val prevEntryTerm: Int) : Command

    class AppendEntityResponse(override val destAlias: String,
                               override val sourceAlias: String,
                               val lastTerm: Int,
                               val lastIndex: Int,
                               val isAppended: Boolean) : Command

    class RequestVote(override val destAlias: String,
                      override val sourceAlias: String,
                      val term: Int,
                      val lastLogTerm: Int,
                      val lastLogIndex: Int) : Command {
        
    }

    class RequestVoteResponse(override val destAlias: String,
                              override val sourceAlias: String,
                              val term: Int,
                              val isVoted: Boolean) : Command {
        
    }
}


