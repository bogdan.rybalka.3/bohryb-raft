package com.bohryb.raft.commands

import java.util.concurrent.ArrayBlockingQueue

class CommandQueue<T>(private val queue: ArrayBlockingQueue<T>) {
    fun get(): T {
        return queue.poll()
    }

    val isEmpty: Boolean
        get() = queue.isEmpty()

    fun put(e: T) {
        queue.put(e)
    }
}