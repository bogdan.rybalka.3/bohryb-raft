package com.bohryb.raft

data class Config (
        var instanceAlias: String,
        var heartbeatTimeoutMin: Long,
        var heartbeatTimeoutMax: Long,
        var electionTimeoutMin: Long,
        var electionTimeoutMax: Long,
        var peers: Map<String, Any>,
    ) {

        constructor() : this("", 0, 0, 0,0, HashMap()) {
        }
}