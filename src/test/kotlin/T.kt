import com.bohryb.raft.Config
import com.bohryb.raft.RaftNode
import com.bohryb.raft.commands.Command
import com.bohryb.raft.commands.CommandQueue
import com.bohryb.raft.sminstructions.SMInstruction
import com.fasterxml.jackson.databind.ObjectMapper
import java.io.IOException
import java.util.concurrent.ArrayBlockingQueue
import java.util.function.BiConsumer
import java.util.function.Consumer

object DistributedRaftImitation {
    @Throws(IOException::class)
    @JvmStatic
    fun main(args: Array<String>) {
        val peers = 3
        val nodeChannels: Map<String, Pair<CommandQueue<Command>, CommandQueue<Command>>> = generatePeers(peers)
        val stringRaftNodeMap = generateRaftNodes(nodeChannels)
        stringRaftNodeMap.values.forEach(Consumer { s: RaftNode ->
            Thread{ s.start() }.start();
        });
        while (true) {
            nodeChannels.forEach({ key: String, value: Pair<CommandQueue<Command>, CommandQueue<Command>> ->
                val resp: CommandQueue<Command> = value.second
                while (!resp.isEmpty) {
                    val command = resp.get()
                    nodeChannels[command.destAlias]!!.first.put(command)
                }
            })
        }
    }

    @Throws(IOException::class)
    fun generateRaftNodes(peers: Map<String, Pair<CommandQueue<Command>, CommandQueue<Command>>>): Map<String, RaftNode> {
        val nodes = HashMap<String, RaftNode>()
        for ((key, value) in peers) {
            val config = ObjectMapper()
                .reader()
                .readValue(
                    DistributedRaftImitation::class.java.classLoader.getResourceAsStream("config.json"),
                    Config::class.java
                ).copy(instanceAlias = key)

            val objectRaftNode = RaftNode(
                value.first,
                value.second,
                config
            )
            nodes[key] = objectRaftNode
        }
        return nodes
    }

    fun generatePeers(count: Int): Map<String, Pair<CommandQueue<Command>, CommandQueue<Command>>> {
        val peers: HashMap<String, Pair<CommandQueue<Command>, CommandQueue<Command>>> =
            HashMap()
        for (i in 1 until count + 1) {
            val alias = "instance_$i"
            val req = CommandQueue(ArrayBlockingQueue<Command>(1000))
            val res = CommandQueue(ArrayBlockingQueue<Command>(1000))
            peers[alias] = Pair(req, res)
        }
        return peers
    }
}
