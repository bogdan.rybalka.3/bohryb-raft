import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.4.32"
}

group = "me.bogdanrybalka"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
//    <dependency>
//    <groupId>com.fasterxml.jackson.core</groupId>
//    <artifactId>jackson-databind</artifactId>
//    <version>${jackson.version}</version>
//    </dependency>
    implementation("com.fasterxml.jackson.core", "jackson-databind", "2.12.3")
    testImplementation(kotlin("test-junit"))
}

tasks.test {
    useJUnit()
}

tasks.withType<KotlinCompile>() {
    kotlinOptions.jvmTarget = "1.8"
}